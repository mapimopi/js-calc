calculate = ->
  width = Number($("input[name='width']").val())
  length = Number($("input[name='length']").val())
  amount = Number($("input[name='amount']").val())
  
  total_meterage = width * length * amount

  quality = $("#quality").val()
  
  days = 100
  days = 200 if total_meterage > 10
  days *= 2 if quality is '300'
  
  $(".price").html("Цена: #{total_meterage}")
  $(".time").html("#{days} дней")
  
  if total_meterage is 0
    $(".time").hide()
  else
    $(".time").show()
  

jQuery ->
  $("form").change ->
    calculate()